import torch
import os
import cv2
import numpy as np
from utils import one_hot_encode
from utils import colour_code_segmentation, reverse_one_hot, visualize_and_save
from params import IMG_PART, img_params
import random


class SegmentationDataset(torch.utils.data.Dataset):

    """Read images, apply augmentation and preprocessing transformations.

    Args:
        images_dir (str): path to images folder
        masks_dir (str): path to segmentation masks folder
        class_rgb_values (list): RGB values of select classes to extract from
                                 segmentation mask
        augmentation (albumentations.Compose): data transfromation pipeline
            (e.g. flip, scale, etc.)
        preprocessing (albumentations.Compose): data preprocessing
            (e.g. noralization, shape manipulation, etc.)

    """

    def __init__(
        self,
        images_dir,
        masks_dir,
        class_rgb_values=None,
        augmentation=None,
        preprocessing=None,
        split_indexes=None,
    ):
        image_names = np.array(sorted(os.listdir(images_dir)))
        masks_names = np.array(sorted(os.listdir(masks_dir)))
        if split_indexes is not None:
            image_names = image_names[split_indexes]
            masks_names = masks_names[split_indexes]
        self.image_paths = [
            os.path.join(images_dir, image_id) for image_id in image_names
        ]
        self.mask_paths = [
            os.path.join(masks_dir, image_id) for image_id in masks_names
        ]

        self.class_rgb_values = class_rgb_values
        self.augmentation = augmentation
        self.preprocessing = preprocessing

    def __getitem__(self, i):
        # read images and masks
        image = cv2.cvtColor(cv2.imread(self.image_paths[i]), cv2.COLOR_BGR2RGB)
        mask = cv2.cvtColor(cv2.imread(self.mask_paths[i]), cv2.COLOR_BGR2RGB)

        # one-hot-encode the mask
        mask = one_hot_encode(mask, self.class_rgb_values).astype("float")

        # apply augmentations
        if self.augmentation:
            sample = self.augmentation(image=image, mask=mask)
            image, mask = sample["image"], sample["mask"]

        # apply preprocessing
        if self.preprocessing:
            sample = self.preprocessing(image=image, mask=mask)
            image, mask = sample["image"], sample["mask"]

        return image, mask

    def __len__(self):
        # return length of
        return len(self.image_paths)


if __name__ == "__main__":
    if not os.path.exists("look_dataset/"):
        os.makedirs("look_dataset/")

    x_train_dir = os.path.join(img_params[IMG_PART]["DATA_DIR"], "train/img")
    y_train_dir = os.path.join(img_params[IMG_PART]["DATA_DIR"], "train/mask")
    select_class_rgb_values = img_params[IMG_PART]["select_class_rgb_values"]

    dataset = SegmentationDataset(
        x_train_dir, y_train_dir, class_rgb_values=select_class_rgb_values
    )
    random_idx = random.randint(0, len(dataset) - 1)
    image, mask = dataset[random_idx]
    visualize_and_save(
        num=random_idx,
        original_image=image,
        dir_name="look_dataset",
        ground_truth_mask=colour_code_segmentation(
            reverse_one_hot(mask), select_class_rgb_values
        ),
    )

import os
from params import ROOM_PART

if __name__ == "__main__":
    dirname = "data_" + ROOM_PART + "/train/"
    img_path = "../" + dirname + "img/"
    mask_path = "../" + dirname + "mask/"
    no_mask_path = "../data_" + ROOM_PART + "/no_mask/"
    mask_names = []
    for filename_ in os.listdir(mask_path):
        filename, extension = os.path.splitext(filename_)
        mask_names.append(filename)

    # for filename_ in os.listdir(img_path):
    #     filename, extension= os.path.splitext(filename_)
    #     if filename not in mask_names:
    #         os.remove(img_path+filename_)

    for filename_ in os.listdir(no_mask_path):
        filename, extension = os.path.splitext(filename_)
        if filename in mask_names:
            os.remove(no_mask_path + filename_)

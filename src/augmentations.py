import albumentations as album
from params import IMG_PART, img_params
from dataset import SegmentationDataset
import random
from utils import colour_code_segmentation, reverse_one_hot, visualize_and_save
import os


def get_training_augmentation():
    if IMG_PART == "main":
        train_transform = [
            album.SmallestMaxSize(max_size=1024, interpolation=1, p=1),
            album.RandomCrop(height=256, width=256, always_apply=True),
            album.HorizontalFlip(p=0.5),
            album.RandomBrightnessContrast(
                brightness_limit=0.1, contrast_limit=0.1, brightness_by_max=True, p=0.5
            ),
        ]
    return album.Compose(train_transform)


def get_validation_augmentation():
    if IMG_PART == "main":
        test_transform = [
            album.Resize(height=1024, width=1792, interpolation=1, p=1),
        ]
    return album.Compose(test_transform)


def get_test_augmentation():
    if IMG_PART == "main":
        test_transform = [
            album.Resize(height=1024, width=1792, interpolation=1, p=1),
        ]
    return album.Compose(test_transform)


def to_tensor(x, **kwargs):
    return x.transpose(2, 0, 1).astype("float32")


def get_preprocessing(preprocessing_fn=None):
    """Construct preprocessing transform
    Args:
        preprocessing_fn (callable): data normalization function
            (can be specific for each pretrained neural network)
    Return:
        transform: albumentations.Compose
    """
    _transform = []
    if preprocessing_fn:
        _transform.append(album.Lambda(image=preprocessing_fn))
    _transform.append(album.Lambda(image=to_tensor, mask=to_tensor))

    return album.Compose(_transform)


if __name__ == "__main__":
    if not os.path.exists("augmentations_examples/"):
        os.makedirs("augmentations_examples/")

    x_train_dir = os.path.join(img_params[IMG_PART]["DATA_DIR"], "train/img")
    y_train_dir = os.path.join(img_params[IMG_PART]["DATA_DIR"], "train/mask")
    select_class_rgb_values = img_params[IMG_PART]["select_class_rgb_values"]

    augmented_dataset = SegmentationDataset(
        x_train_dir,
        y_train_dir,
        augmentation=get_training_augmentation(),
        class_rgb_values=select_class_rgb_values,
    )

    random_idx = random.randint(0, len(augmented_dataset) - 1)

    print("idx = ", random_idx)
    # Different augmentations on a random image/mask pair (256*256 crop)
    for idx in range(3):
        image, mask = augmented_dataset[random_idx]
        visualize_and_save(
            num=idx,
            dir_name="augmentations_examples",
            original_image=image,
            ground_truth_mask=colour_code_segmentation(
                reverse_one_hot(mask), select_class_rgb_values
            ),
        )

import segmentation_models_pytorch as smp
import click

from params import (
    img_params,
    IMG_PART,
    ENCODER,
    ENCODER_WEIGHTS,
    CLASSES,
    ACTIVATION,
)
from dataset import SegmentationDataset
from augmentations import (
    get_training_augmentation,
    get_validation_augmentation,
    get_preprocessing,
)
from utils import (
    colour_code_segmentation,
    reverse_one_hot,
    visualize_and_save,
)
from torch.utils.data import DataLoader
import torch
import os
import numpy as np


@click.command()
@click.argument("epoch", type=int, default=300)
@click.argument("batch_size", type=int, default=18)
@click.argument("training", type=int, default=True)
def train(epoch, batch_size, training):
    """
    Обучение модели.

        Параметры:
            epoch (int): количество эпох
            batch_size (int): размер батча
            training (bool): если False, то только валидация модели

    """
    print(f"Epochs = {epoch}")
    print(f"atch size = {batch_size}")

    model = smp.DeepLabV3Plus(
        encoder_name=ENCODER,
        encoder_weights=ENCODER_WEIGHTS,
        classes=len(CLASSES),
        activation=ACTIVATION,
    )

    preprocessing_fn = smp.encoders.get_preprocessing_fn(ENCODER, ENCODER_WEIGHTS)

    x_train_dir = os.path.join(img_params[IMG_PART]["DATA_DIR"], "train/img")
    y_train_dir = os.path.join(img_params[IMG_PART]["DATA_DIR"], "train/mask")
    x_test_dir = os.path.join(img_params[IMG_PART]["DATA_DIR"], "test/img")
    y_test_dir = os.path.join(img_params[IMG_PART]["DATA_DIR"], "test/mask")
    select_class_rgb_values = img_params[IMG_PART]["select_class_rgb_values"]
    # Get train and val dataset instances
    train_dataset = SegmentationDataset(
        x_train_dir,
        y_train_dir,
        augmentation=get_training_augmentation(),
        preprocessing=get_preprocessing(preprocessing_fn),
        class_rgb_values=select_class_rgb_values,
    )

    valid_dataset = SegmentationDataset(
        x_test_dir,
        y_test_dir,
        augmentation=get_validation_augmentation(),
        preprocessing=get_preprocessing(preprocessing_fn),
        class_rgb_values=select_class_rgb_values,
    )

    # Get train and val data loaders
    train_loader = DataLoader(
        train_dataset, batch_size=batch_size, shuffle=True, num_workers=0
    )
    valid_loader = DataLoader(valid_dataset, batch_size=1, shuffle=False, num_workers=0)
    valid_dataset_vis = SegmentationDataset(
        x_test_dir,
        y_test_dir,
        augmentation=get_validation_augmentation(),
        class_rgb_values=select_class_rgb_values,
    )

    # Set device: `cuda` or `cpu`
    DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # define loss function
    loss = smp.utils.losses.DiceLoss()

    # define metrics
    metrics = [
        smp.utils.metrics.IoU(threshold=0.5),
    ]

    # define optimizer
    optimizer = torch.optim.Adam(
        [
            dict(params=model.parameters(), lr=0.00008),
        ]
    )

    # load best saved model checkpoint from previous commit (if present)
    if os.path.exists("../weights/best_model_sd.pth"):
        print("Start from checkpoint!")
        checkpoint = torch.load("../best_model_sd.pth", map_location=DEVICE)
        model.load_state_dict(checkpoint)

    if not os.path.exists("../weights/best_models/"):
        os.makedirs("../weights/best_models/")

    train_epoch = smp.utils.train.TrainEpoch(
        model,
        loss=loss,
        metrics=metrics,
        optimizer=optimizer,
        device=DEVICE,
        verbose=True,
    )

    valid_epoch = smp.utils.train.ValidEpoch(
        model,
        loss=loss,
        metrics=metrics,
        device=DEVICE,
        verbose=True,
    )

    print(len(train_dataset))

    if training:
        train_logs_list, valid_logs_list = [], []

        for i in range(1, epoch + 1):
            # Perform training & validation
            print("\nEpoch: {}".format(i))
            train_logs = train_epoch.run(train_loader)
            valid_logs = valid_epoch.run(valid_loader)
            train_logs_list.append(train_logs)
            valid_logs_list.append(valid_logs)

            if i % 100 == 0:
                torch.save(
                    model.state_dict(),
                    "../best_models/best_model_sd" + str(i) + "ep.pth",
                )
                print("Model saved!")

        if i > 10:
            torch.save(
                model.state_dict(), "../best_models/best_model_sd" + str(i) + "ep.pth"
            )

    model.eval()

    VAL_OUTPUT = "../data/interim/validation_output/"
    for idx in range(len(valid_dataset)):
        if not os.path.exists(VAL_OUTPUT):
            os.makedirs(VAL_OUTPUT)

        image, gt_mask = valid_dataset[idx]
        image_vis = valid_dataset_vis[idx][0].astype("uint8")
        x_tensor = torch.from_numpy(image).to(DEVICE).unsqueeze(0)
        # Predict test image
        with torch.no_grad():
            pred_mask = model(x_tensor)
        pred_mask = pred_mask.detach().squeeze().cpu().numpy()
        # Convert pred_mask from `CHW` format to `HWC` format
        pred_mask = np.transpose(pred_mask, (1, 2, 0))
        pred_mask = colour_code_segmentation(
            reverse_one_hot(pred_mask), select_class_rgb_values
        )
        # Convert gt_mask from `CHW` format to `HWC` format
        gt_mask = np.transpose(gt_mask, (1, 2, 0))
        gt_mask = colour_code_segmentation(
            reverse_one_hot(gt_mask), select_class_rgb_values
        )

        visualize_and_save(
            num=idx,
            dir_name=VAL_OUTPUT,
            original_image=image_vis,
            ground_truth_mask=gt_mask,
            predicted_mask=pred_mask,
        )


if __name__ == "__main__":
    train()

import numpy as np

IMG_PART = "main"

img_params = {}
img_params["main"] = {
    "DATA_DIR": "../data/processed/",
    "weigths": "../weigths/outdoor-wp-main-4-class.pth",
    "select_classes": ["background", "concrete", "formwork", "reinforcement"],
    "select_class_rgb_values": np.array(
        [
            [0, 0, 0],
            [0, 255, 0],
            [255, 0, 0],
            [0, 0, 255],
        ]
    ),
}

# model params
ENCODER = "resnet50"
ENCODER_WEIGHTS = "imagenet"
CLASSES = img_params[IMG_PART]["select_classes"]
ACTIVATION = "sigmoid"

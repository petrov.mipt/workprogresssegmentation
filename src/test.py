import segmentation_models_pytorch as smp
from imgaug import SegmentationMapsOnImage
from dataset import SegmentationDataset
from params import (
    img_params,
    IMG_PART,
    ENCODER,
    ENCODER_WEIGHTS,
    CLASSES,
    ACTIVATION,
)
from utils import colour_code_segmentation, reverse_one_hot, visualize_and_save
import os
from augmentations import get_test_augmentation, get_preprocessing
from torch.utils.data import DataLoader
import torch
import numpy as np


def get_class_shares(pred_mask, select_classes, select_class_rgb_values):
    total_pixels = pred_mask.shape[0] * pred_mask.shape[1]
    count_of_pixels_for_classes = np.zeros(len(select_classes))
    class_shares = np.zeros(len(select_classes))
    for i in range(len(select_classes)):
        count_of_pixels_for_classes[i] = np.count_nonzero(
            np.all(pred_mask == select_class_rgb_values[i], axis=2)
        )
        class_shares[i] = count_of_pixels_for_classes[i] / total_pixels
        if class_shares[i] > 0.02:
            print("{} : {}".format(select_classes[i], class_shares[i]))


def crop_image(image, target_image_dims):
    target_size_h = target_image_dims[0]
    target_size_w = target_image_dims[1]
    image_size_h = image.shape[0]
    image_size_w = image.shape[1]
    padding_h = (image_size_h - target_size_h) // 2
    padding_w = (image_size_w - target_size_w) // 2
    s_h = (image_size_h - target_size_h) % 2
    s_w = (image_size_w - target_size_w) % 2

    return image[
        padding_h : image_size_h - padding_h - s_h,
        padding_w : image_size_w - padding_w - s_w,
    ]


def set_default_colors_to_mask(mask):
    mask.DEFAULT_SEGMENT_COLORS[0] = (0, 0, 0)
    opacity = 100
    if IMG_PART == "main":
        mask.DEFAULT_SEGMENT_COLORS[1] = (opacity, 255, opacity)
        mask.DEFAULT_SEGMENT_COLORS[2] = (255, opacity, opacity)
        mask.DEFAULT_SEGMENT_COLORS[3] = (opacity, opacity, 255)


if __name__ == "__main__":
    DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    x_test_dir_additional = os.path.join(img_params[IMG_PART]["DATA_DIR"], "no_mask")
    select_class_rgb_values = img_params[IMG_PART]["select_class_rgb_values"]

    preprocessing_fn = smp.encoders.get_preprocessing_fn(ENCODER, ENCODER_WEIGHTS)

    test_dataset = SegmentationDataset(
        x_test_dir_additional,
        x_test_dir_additional,
        augmentation=get_test_augmentation(),
        preprocessing=get_preprocessing(preprocessing_fn),
        class_rgb_values=select_class_rgb_values,
    )

    test_dataset_vis = SegmentationDataset(
        x_test_dir_additional,
        x_test_dir_additional,
        augmentation=get_test_augmentation(),
        class_rgb_values=select_class_rgb_values,
    )
    test_loader = DataLoader(test_dataset, batch_size=1, shuffle=False, num_workers=0)

    model = smp.DeepLabV3Plus(
        encoder_name=ENCODER,
        encoder_weights=ENCODER_WEIGHTS,
        classes=len(CLASSES),
        activation=ACTIVATION,
    )

    checkpoint = torch.load(img_params[IMG_PART]["weigths"], map_location=DEVICE)
    model.load_state_dict(checkpoint)
    model.to(DEVICE)

    model.eval()
    for idx in range(len(test_dataset)):
        if not os.path.exists("../out/"):
            os.makedirs("../out/")

        image, gt_mask = test_dataset[idx]
        img_name = os.path.splitext(os.path.basename(test_dataset.image_paths[idx]))[0]
        image_vis = test_dataset_vis[idx][0].astype("uint8")
        x_tensor = torch.from_numpy(image).to(DEVICE).unsqueeze(0)
        # Predict test image
        with torch.no_grad():
            pred_mask = model(x_tensor)
        pred_mask = pred_mask.detach().squeeze().cpu().numpy()
        # Convert pred_mask from `CHW` format to `HWC` format
        pred_mask = np.transpose(pred_mask, (1, 2, 0))
        # Get prediction channel corresponding to road
        pred_mask = reverse_one_hot(pred_mask)
        mask = SegmentationMapsOnImage(
            pred_mask.astype(np.int32),
            shape=image_vis.shape[0:2],
        )
        set_default_colors_to_mask(mask)
        pred_mask = colour_code_segmentation(pred_mask, select_class_rgb_values)
        get_class_shares(pred_mask, CLASSES, select_class_rgb_values)
        # Convert gt_mask from `CHW` format to `HWC` format
        gt_mask = np.transpose(gt_mask, (1, 2, 0))
        gt_mask = reverse_one_hot(gt_mask)
        mask_gt = SegmentationMapsOnImage(
            gt_mask.astype(np.int32),
            shape=image_vis.shape[0:2],
        )
        set_default_colors_to_mask(mask_gt)
        gt_mask = colour_code_segmentation(gt_mask, select_class_rgb_values)

        visualize_and_save(
            num=idx,
            dir_name="../out",
            original_image=image_vis,
            predicted_mask=mask.draw_on_image(image_vis)[0],
        )
